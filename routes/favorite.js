const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

const favorite = require('../models/favorite');
const authenticate = require('../authenticate');
const cors = require('./cors');
const favoriteRouter = express.Router();

favoriteRouter.use(bodyParser.json());

favoriteRouter.route('/')
    .get(cors.cors, authenticate.verifyUser, (req, res, next) => {
        favorite.find({ 'author': req.user._id })
            .populate('author')
            .populate('dishes')
            .then((favorites) => {
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                res.json(favorites);
            }, (err) => next(err))
            .catch((err) => next(err));
    })
    .post(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
        favorite.findOne({ 'author': req.user._id })
            .then((favorites) => {
                if (favorites) {
                  
                    if (favorites.dishes.indexOf(req.body._id[i]) == -1) {
                        favorites.dishes.push(req.body._id[i]);
                        favorites.save();
                    }
                 
                    res.statusCode = 200;
                    res.setHeader('Content-Type', 'application/json');
                    res.json(favorites);
                } else {
                    favorite.create({ 'author': req.user._id, 'dishes': req.body._id })
                        .then((favorites) => {
                            res.statusCode = 200;
                            res.setHeader('Content-Type', 'application/json');
                            res.json(favorites);
                        }, (err) => next(err))
                        .catch((err) => next(err));
                }

            }, err => next(err));


    })
    .delete(authenticate.verifyUser, (req, res, next) => {
        favorite.remove({ 'author': req.user._id })
            .then((resp) => {
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                res.json(resp);
            })
            .catch((err) => next(err));
    });

favoriteRouter.route('/:dishId')
    .post(authenticate.verifyUser, (req, res, next) => {
        favorite.findOne({ 'author': req.user._id })
            .then((favorites) => {
                if (favorites) {
                    console.log("favorites ", favorites);
                    if (favorites.dishes.indexOf(req.params.dishId) == -1) {
                        favorites.dishes.push(req.params.dishId);
                        favorites.save();
                    }
                    res.statusCode = 200;
                    res.setHeader('Content-Type', 'application/json');
                    res.json(favorites);
                } else {
                    favorite.create({ 'author': req.user._id, 'dishes': req.params.dishId })
                        .then((favorites) => {
                            res.statusCode = 200;
                            res.setHeader('Content-Type', 'application/json');
                            res.json(favorites);
                        }, (err) => next(err))
                        .catch((err) => next(err));
                }

            }, err => next(err));
    })
    .delete(authenticate.verifyUser, (req, res, next) => {
        favorite.findOne({ 'author': req.user._id })
            .then((favorites) => {
                if (favorites) {
                    var index = favorites.dishes.indexOf(req.params.dishId);
                    if (index > -1) {
                        favorites.dishes.splice(index, 1);
                        favorites.save();
                    }
                    res.statusCode = 200;
                    res.setHeader('Content-Type', 'application/json');
                    res.json(favorites);
                }
            }, (err) => next(err))
            .catch((err) => next(err));
    });

module.exports = favoriteRouter;