const mongoose = require('mongoose');
const Schema = mongoose.Schema;
require('mongoose-currency').loadType(mongoose);
var Currency = mongoose.Types.Currency;

var promotionSchema = new Schema({
    name :{
        type:String,
        required: true,   
    },
    image:{
        type:String,
        required:true
    },
    label:{
        type:String , 
        default:""
    },
    price:{
        type:Currency,
        required:true
    },
    description:{
        type:String
    },
    featured:{
        type:Boolean,
        default:false
    }
});

var promotions = mongoose.model('Promotion',promotionSchema);
module.exports =promotions;