const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const favoriteShema = new Schema({
   author:{
       type:mongoose.Schema.Types.ObjectId,
       ref:"User"
   },
   dishes:[{
       type:mongoose.Schema.Types.ObjectId,
       ref:"Dish"
   }]
});

var favoirtes = mongoose.model("Favorite",favoriteShema);

module.exports = favoirtes;